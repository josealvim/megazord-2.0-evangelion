#!/bin/bash

# Variáveis que são necessárias para o cadastro.
CN=""
NID=""
USRNAME=""
PASS=""
MAIL=""
GID=""
CLIMODE="false"

function ERROR() {
	echo -e "[ERROR] $1"
}

function showHelp() {
        echo -e "Usage: $0 --cli | - | [OPTIONS]"
        echo -e "\t--cli    -  : Modo interativo, pergunta cada item"
        echo -e "\t--nid,	-n : \"Número USP\" "
        echo -e "\t--uid,	-u : \"Login\""
        echo -e "\t--pass, 	-p : \"Senha\""
        echo -e "\t--cname,	-c : \"Nome Completo\""
        echo -e "\t--mail,	-e : \"Email\""
        echo -e "\t--gid,	-g : \"Curso\", bm | lic | licn | bma | bmac | bcc | spec | prof | be"
}

if [ "$#" -eq 0 ]; then
	showHelp
	exit 0
fi

# LDAP NEEDS THIS AND ALSO WHAT
while ( ! klist -s ); do 
	batatao="$USER"
	
	while [ "$batatao" == "root" ]; do
		echo "Digite seu usuario, Admin tonto:"
		echo "> "
		read -s batatao
	done

	kinit -p "$batatao/admin"
done

for arg in "$@"; do
	case $arg in 
		"--help" | "-h")
			showHelp
			exit 0;;
			
	    "--cli"  | "-")
	       CLIMODE="true";;
	esac
done

if [ "$CLIMODE" == "false" ]; then
    # PARSE THIS SHITTOS
    
    option_name=""
    for arg in "$@"; do
    
    	if [ -z "$option_name" ]; then
    		option_name="$arg"
    		continue
    	fi
    
    	case $option_name in 
    		"--uid" 	| "-u")
    			USRNAME="$arg";;
    
    		"--mail" 	| "-e")
    			MAIL="$arg";;
    
    		"--pass"	| "-p")
    			PASS="$arg";;
    
    		"--cname" 	| "-c")
    			CN="$arg";;
    
    		"--nid" 	| "-n")
    			NID="$arg";;
    
    		"--gid" 	| "-g")
    			GID="$arg";;
    
    		*)
    			ERROR "Parse Error: No such option: \"$option_name\""
    			showHelp
    			exit 1
    	esac
    
    	option_name=""
    done
fi

# GET AND CHECK NID
while [ -z "$NID" ]; do
	echo "Digite o número USP:"
	echo -n "> "
	read NID

	if ldapsearch nid=$NID | fgrep nid: > /dev/null; then
		ERROR "Número USP já está sendo utilizado."
		NID=""
		continue
	fi
done

# GET COMMON NAME
while [ -z "$CN" ]; do 
	echo "Digite o Nome Completo:"
	echo -n "> "
	read CN
done

CN=$(echo "$CN" | 
    sed 's/[áàâäãå]/a/' |
    sed 's/[ÁÀÂÄÃÅ]/A/' |
    sed 's/[éèêëẽ]/e/'  | 
    sed 's/[ÉÈÊËẼ]/E/'  |
    sed 's/[íìîïĩ]/i/'  |
    sed 's/[ÍÌÎÏĨ]/I/'  |
    sed 's/[óòôöõø]/o/' |
    sed 's/[ÓÒÔÖÕØ]/O/' |
    sed 's/[úùûüũ]/u/'  |
    sed 's/[ÚÙÛÜŨ]/U/'  |
    sed 's/[ÚÙÛÜŨ]/U/'  |
    sed 's/[ýỳŷÿỹ]/y/'  |
    sed 's/[ÝỲŶŸỸ]/Y/'  |
    sed 's/[ńǹñ]/n/'    |
    sed 's/[ŃǸÑ]/N/'    |
    sed 's/[æǽ]/ae/'    |
    sed 's/[ÆǼ]/AE/'    |
    sed 's/[ćĉç]/c/'    |
    sed 's/[ĆĈÇ]/C/'    )

# GET GROUP ID
while [ -z "$GID" ]; do
	echo "Entre com o Curso/Grupo:"
	echo "bm | lic | licn | bma | bmac | bcc | spec | prof | be" 
	echo -n "> "
	read GID

	case $GID in
		"bm"	| \
		"lic"	| \
		"licn"	| \
		"bma"	| \
		"bmac"	| \
		"bcc"	| \
		"spec"	| \
		"prof"	| \
		"be")
			#nop
		;;

		*)
			ERROR "Grupo inexistente!"
			GID=""
			continue
	esac
done

# GET AND CHECK USER
while [ -z "$USRNAME" ]; do
	echo "Escolha um nome de login:"
	echo "Apenas Letras Minúsculas."
	echo -n "> "
	read USRNAME
    
    if [[ ! "$USRNAME" =~ ^[a-z]{2,12}$ ]]; then
		ERROR "Seu Usuário deve conter entre 2 e 12 letras minúsculas."
		USRNAME=""
		continue
	fi

	if id -u $USRNAME > /dev/null 2>&1; then
		ERROR "Nome $USRNAME não está disponível. ID"
		USRNAME=""
		continue
	fi

	LDAPDATA=$(ldapsearch uid=\"$USRNAME\" -LLL | grep \"uid: $USRNAME\")
	if [ ! -z "$LDAPDATA" ]; then
		ERROR "Nome $USRNAME não está disponível. LDAP"
		USRNAME=""
		continue
	fi

	INFO=$(cat forbidden_uids | grep $USRNAME)
	if [ ! -z "$INFO" ]; then
		ERROR "Nome $USRNAME está proíbido."
        ERROR "$(echo $INFO | cut -d'|' -f2 | sed 's/[ \t]//g')"
		USRNAME=""
		continue
	fi
done

# GET EMAIL
if [ -z "$MAIL" ]; then
	echo "Email para contato:"
	echo -n "> "
	read MAIL

	if [ -z "$MAIL" ]; then
		MAIL="$USRNAME@linux.ime.usp.br"
	else
		MAIL="$MAIL,$USRNAME@linux.ime.usp.br"
	fi
fi

if [ -z "$PASS" ]; then
	CONF="_not_empty_"

	while [ "$PASS" != "$CONF" ]; do
		echo "Senha:"
		echo -n "> "
		read -s PASS

		echo "Confirmar Senha:"
		echo -n "> "
		read -s CONF

		if [ "$CONF" != "$PASS" ]; then
			ERROR "Senhas não batem."
		fi
	done
fi

echo "Última Chance de Cancelamento"
echo -e "Criação em: "

for number in {1..10}; do
	echo -en "$((10-${number}))"
	sleep 1
	echo -en "\b \b"
done

echo "Criando..."

##### LDAP
function getUidNumberLdap {
	n=0
	for i in $(ldapsearch -x -LLL "(uidNumber=*)" uidNumber -S uidNumber | grep uidNumber | tail -n1 )
	do
		ldaparry[$n]=$i
		let n+=1
	done
    if [ "${ldaparry[0]}" == "uidNumber:" ]
    then
		echo $((${ldaparry[1]}+1))
		return 0
    else
		return -1
    fi
}

NUMBERUID=$(getUidNumberLdap)
GUID=$(ldapsearch -x cn=$GID | fgrep gidNumber | awk '{ print $2 }' | tr -d '[[:space:]]')

#cat <<EOF > /root/hist/"$USRNAME"
#dn: uid=$USRNAME,ou=People,dc=linux,dc=ime,dc=usp,dc=br
#uid: $USRNAME
#objectClass: account
#objectClass: posixAccount
#objectClass: top
#objectClass: pessoaLinux
#loginShell: /bin/bash
#homeDirectory: /home/$GID/$USRNAME
#uidNumber: $NUMBERUID
#gidNumber: $GUID
#cn: $CN
#nid: $NID
#EOF
#ldapadd -f /root/hist/$USRNAME

echo $CN

cat <<EOF > /root/hist/"$USRNAME"
dn: uid=$USRNAME,ou=People,dc=linux,dc=ime,dc=usp,dc=br
uid: $USRNAME
objectClass: account
objectClass: posixAccount
objectClass: top
objectClass: pessoaLinux
loginShell: /bin/bash
homeDirectory: /home/$GID/$USRNAME
uidNumber: $NUMBERUID
gidNumber: $GUID
cn: $CN
gecos: $CN
nusp: $NID
EOF
ldapadd -f /root/hist/$USRNAME

ldapmodify <<EOF
dn: uid=$USRNAME,ou=People,dc=linux,dc=ime,dc=usp,dc=br
changetype: modify
add: mail
mail: $MAIL
EOF

##### CYRUS
# Cria o email do usuário com cota de 256 MB
cyradm -u cyrus -a cyrus --auth PLAIN -w $(cat /root/SECRETS/cyrusPasswd) --server hermes.linux.ime.usp.br << EOF 2>&1 > /dev/null
createmailbox user.$USRNAME
setquota user.$USRNAME 256000
quit
EOF

##### NFS
ssh -i /root/megazord_id_rsa root@nfs "/bin/bash /megazord/scripts/criaHome.sh \"$USRNAME\" \"$GID\""

##### LISTAS DE EMAILS
ssh -i /root/megazord_id_rsa root@lists "/bin/bash /megazord/scripts/adicionaListaEmails.sh \"$USRNAME\" \"$GID\""

##### KERBEROS
kadmin -p megazord/admin -k -t /root/mega.keytab -q "addprinc -randkey $USRNAME" 2>&1 > /dev/null
kadmin -p megazord/admin -k -t /root/mega.keytab -q "change_password -pw $PASS $USRNAME" > /dev/null

service sssd restart > /dev/null

echo "Feito!"

exit 0
