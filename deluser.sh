#!/bin/bash

# Variáveis que são necessárias para a deleção
USERNAME="$1"
GROUP=""
DELADM=""
DELPROF=""
NOWAIT=""

function ERROR() {
    echo -e "[ERROR] $1" 
}

function showHelp() {
    echo -e "Usage: $0 \$uid [options]"
    echo -e "\t--help -h ?\tmostra esta tela"
    echo -e "\t--gnann \thabilita deleção de admins"
    echo -e "\t--fodaçe\tdesabilita o tempo de cancelamento da deleção"
    echo -e "\t--odilon\thabilita deleção de professores"
}

if [ $# -eq 0 ]; then 
    showHelp
    exit 0
fi

for var in $@; do
    if [ "$var" == "--gnann" ]; then
        DELADM="true"
    fi
    
    if [ "$var" == "--fodaçe" ]; then
        NOWAIT="true"
    fi
    
    if [ "$var" == "--odilon" ]; then
        DELPROF="true"
    fi

    if [[ "$var" == "--help" || "$var" == "-h" || "$var" == "?" ]]; then
        showHelp
        exit 0
    fi
done

if [[ ! "$USERNAME" =~ ([a-z]*) ]]; then
    ERROR "invalid user $USERNAME"
    exit 1
fi

# Check if user exist.
if id -u "$USERNAME" >/dev/null 2>&1; then
    echo "User $USERNAME found"
else
    ERROR "User $USERNAME unknown"
    exit 1
fi

#ldap etc needs this
while ( ! klist -s ); do 
    batatao="$USER"
                
    while [ "$batatao" == "root" ]; do
        echo "Digite seu usuario, Admin tonto:"
        echo "> "
        read -s batatao
    done

    kinit -p "$batatao/admin"
done

echo "Getting group..."
GIDNUMBER=$(ldapsearch -LLL -x uid="$USERNAME" | grep "gidNumber:" | cut -d' ' -f2)
GROUPNAME=$(ldapsearch -LLL -x gidNumber=$GIDNUMBER \
    -b ou=Group,dc=linux,dc=ime,dc=usp,dc=br | grep "cn: " | cut -d' ' -f2)
if [ -z "$GROUPNAME" ]; then
    ERROR "Usuário com GID inválido: $GIDNUMBER"
    exit 1
fi

echo "Checando Imortais..."
IS_IMORTAL=$(ldapsearch -LLL -x cn="imortais" 2>/tmp/ldaplog | grep "memberUid" | grep "$USERNAME")
IS_PROF=$(ldapsearch -LLL -x cn="prof" 2>/tmp/ldaplog | grep "memberUid" | grep "$USERNAME")

if [[ (! -z "$IS_IMORTAL") && (-z "$DELADM") ]]; then
    ERROR "$USERNAME é um ex admin, veja --help"
    exit -1
fi

if [[ (! -z "$IS_PROF") && (-z "$DELPROF") ]]; then
    ERROR "$USERNAME é um professor, veja --help"
    exit -1
fi

if [ -z "$NOWAIT" ]; then
    echo "Este script não faz o backup da home do usuário"
    echo "Última Chance de Cancelamento"
    echo -e "Deleção em: "

    for number in {1..10}; do
        echo -en "$((10-${number}))"
        sleep 1
        echo -en "\r      \r"
    done
fi

echo "Apertando X no usuário..."

# Delete mail from LDAP before removing from Pykota
#echo "Deleting mail from user's LDAP..."
#ldapmodify << EOF > /tmp/lmodifylog
#dn: uid=$USERNAME,ou=Peopl,dc=linux,dc=ime,dc=usp,dc=br
#changetype: modify
#delete: mail
#EOF

# Remove from megalixo
echo "Deleting user from megalixo..."
ssh megalixo "rm /var/www/quotas/$USERNAME || echo \"Usuário nunca imprimiu\""

# LDAP
echo "Deleting user from LDAP..."
ENTRIES=$(ldapsearch -LLL uid=$USERNAME | grep 'dn:' | cut -d' ' -f2)
for entry in $ENTRIES; do
    ldapdelete "$entry"                 &&  \
    echo -e "Deleted entry:\t$entry"    ||  \
    echo -e "Failed at entry:\t$entry"   
done

# KERBEROS
echo "Deleting kerberos principals..."
kadmin                                      \
    -p megazord/admin                       \
    -k -t /root/mega.keytab                 \
    -q "delprinc -force $USERNAME"        &&     \
    echo "Deleted user's principal!" || 
    echo "Failed deleting principal"

echo "Checking for kereberos admin principal..."
CHECK_ADMIN=$(kadmin                     \
    -p megazord/admin                    \
    -k -t /root/mega.keytab              \
    -q "getprinc $USERNAME/admin" 2>/dev/null |
    grep "$USERNAME/admin"                    )

if [ ! -z "$CHECK_ADMIN" ]; then
    echo "Deleting admin credentials"
    kadmin                                  \
        -p megazord/admin                   \
        -k -t /root/mega.keytab             \
        -q "delprinc -force $USERNAME/admin"  && \
        echo "Deleted user's principal!" || 
        echo "Failed deleting principal"
else
    echo "User had no admin credentials"
fi

# EMAIL (CYRUS)
echo "Deleting mail account..."
cyradm                                  \
    -u cyrus                            \
    -a cyrus                            \
    --auth PLAIN                        \
    -w $(cat /root/SECRETS/cyrusPasswd) \
hermes.linux.ime.usp.br << EOF
setaclmailbox user.$USERNAME cyrus +cd
deletemailbox user.$USERNAME
quit
EOF

# Listas de emails
echo "Removing user from mail groups..."
ssh -i /root/megazord_id_rsa root@lists \
    "/bin/bash /megazord/scripts/removeListaEmails.sh $USERNAME $GROUPNAME"

# NFS
echo "Removing user home..."
ssh -i /root/megazord_id_rsa root@nfs \
    "/bin/bash /megazord/scripts/apagaHome.sh $USERNAME $GROUPNAME"

echo "O usuário $USERNAME foi completamente removido com sucesso."

service sssd restart > /dev/null

exit 0
